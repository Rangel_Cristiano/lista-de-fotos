package com.ucs.photoList.Activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.ucs.photoList.Database.PhotoDao;
import com.ucs.photoList.Fragments.ItemFragment;
import com.ucs.photoList.Models.Photo;
import com.ucs.photoList.R;
import com.ucs.photoList.Utils.Services;

import java.io.File;


public class MainActivity extends AppCompatActivity
        implements ItemFragment.OnListFragmentInteractionListener {

    public static final String EXTRA_ITEM = "item";
    private static final int TAKE_PICTURE = 1;
    private static final int BACK_PHOTO = 2;
    private File mPhoto;
    private MainActivity context;
    private RecyclerView.Adapter recyclerViewAdapter;
    private RecyclerView recyclerView;
    private FloatingActionButton floatingActionButton;
    private ProgressBar spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle("Álbum de Fotos");

        context = this;

        spinner = (ProgressBar)findViewById(R.id.progressBar);
        spinner.setVisibility(View.GONE);

        if (recyclerViewAdapter == null) {
            Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.main_fragment);
            recyclerView = (RecyclerView) currentFragment.getView();
            recyclerViewAdapter = ((RecyclerView) currentFragment.getView()).getAdapter();

            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                    DividerItemDecoration.VERTICAL);

            recyclerView.addItemDecoration(dividerItemDecoration);
        }

        floatingActionButton = findViewById(R.id.fab);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takePhoto();
            }
        });

        Services.verifyStoragePermissions(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_scrolling, menu);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (spinner != null)
            spinner.setVisibility(View.GONE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case TAKE_PICTURE:
                if (resultCode == Activity.RESULT_OK) {

                    if (spinner != null)
                        spinner.setVisibility(View.GONE);

                    //dialog dos campos
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle("Cadastrar Foto");
                    builder.setMessage("Preencha os campos");

                    LinearLayout layout = new LinearLayout(context);
                    layout.setOrientation(LinearLayout.VERTICAL);
                    layout.setPadding(30,5,30,5);

                    final EditText titleBox = new EditText(context);
                    titleBox.setHint("Título");
                    titleBox.setCursorVisible(true);
                    layout.addView(titleBox);

                    final EditText descriptionBox = new EditText(context);
                    descriptionBox.setHint("Descrição");
                    layout.addView(descriptionBox);

                    builder.setView(layout);

                    builder.setPositiveButton("Adicionar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Photo photo = new Photo();
                            photo.setTitle(titleBox.getText().toString());
                            photo.setDescription(descriptionBox.getText().toString());
                            photo.setPath(Uri.fromFile(mPhoto).toString());

                            PhotoDao photoDao = new PhotoDao(context);
                            long id = photoDao.savePhoto(photo);

                            photo.setId(id);

                            Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.main_fragment);
                            if (currentFragment instanceof ItemFragment)
                                ((ItemFragment)currentFragment).addPhoto(photo);
                        }
                    });

                    builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });

                    AlertDialog dialog = builder.create();
                    dialog.setCancelable(false);
                    dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
                    dialog.show();

                }
            case BACK_PHOTO:
                Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.main_fragment);
                if (currentFragment instanceof ItemFragment)
                    ((ItemFragment)currentFragment).notifyDataChanged();
        }
    }

    @Override
    public void onListFragmentInteraction(Photo item) {
        spinner.setVisibility(View.VISIBLE);

        Intent intent = new Intent(this, PhotoDetailActivity.class);
        intent.putExtra(EXTRA_ITEM, item);
        startActivityForResult(intent, BACK_PHOTO);
    }


    public void takePhoto() {
        if (spinner != null)
            spinner.setVisibility(View.VISIBLE);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        long position = new PhotoDao(context).getLastId() + 1;
        mPhoto = new File(Environment.getExternalStorageDirectory(),  position + ".jpg");

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT,
                Uri.fromFile(mPhoto));
        startActivityForResult(intent, TAKE_PICTURE);
    }
}
