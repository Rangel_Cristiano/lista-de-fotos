package com.ucs.photoList.Activities;

import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ucs.photoList.Database.PhotoDao;
import com.ucs.photoList.Models.Photo;
import com.ucs.photoList.R;

import java.io.File;
import java.io.IOException;

public class PhotoDetailActivity extends AppCompatActivity {

    private ImageView mPhotoImageView;
    private EditText mTitleEditView, mDescriptionEditView;
    private TextView mTitleTextView, mDescriptionTextView;
    private LinearLayout mModeEditLinearLayout;
    private Button mSaveButton, mCancelButton;
    private Context mContext;
    private Photo mPhoto;
    private ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        setTitle("Foto");

        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mContext = this;
        mPhotoImageView = findViewById(R.id.photo_view);
        mTitleEditView = findViewById(R.id.title_edit_text);
        mDescriptionEditView = findViewById(R.id.description_edit_text);
        mTitleTextView = findViewById(R.id.title_text);
        mDescriptionTextView = findViewById(R.id.description_text);
        mModeEditLinearLayout = findViewById(R.id.edit_mode);
        mSaveButton = findViewById(R.id.save_button);
        mCancelButton = findViewById(R.id.cancel_button);
        mProgressBar = findViewById(R.id.progressBarDetail);

        mProgressBar.setVisibility(View.GONE);
        mTitleEditView.setVisibility(View.GONE);
        mDescriptionEditView.setVisibility(View.GONE);
        mModeEditLinearLayout.setVisibility(View.GONE);

        mCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editMode(false);
            }
        });

        mSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editMode(false);

                PhotoDao photoDao = new PhotoDao(mContext);

                mPhoto.setTitle(mTitleEditView.getText().toString());
                mPhoto.setDescription(mDescriptionEditView.getText().toString());

                photoDao.updatePhoto(mPhoto);

                mTitleTextView.setText(mPhoto.getTitle());
                mDescriptionTextView.setText(mPhoto.getDescription());

                Toast.makeText(mContext, "As alterações foram salvas com sucesso!", Toast.LENGTH_LONG).show();
            }
        });


        Intent intent = getIntent();
        mPhoto = (Photo) intent.getSerializableExtra(MainActivity.EXTRA_ITEM);

        Uri selectedImage = Uri.parse(mPhoto.getPath());
        Bitmap bitmap;
        try {
            bitmap = android.provider.MediaStore.Images.Media
                    .getBitmap(getContentResolver(), selectedImage);

            mPhotoImageView.setImageBitmap(bitmap);

        } catch (IOException e) {
            e.printStackTrace();
        }

        String title = mPhoto.getTitle() == null || mPhoto.getTitle().isEmpty() ? mPhoto.getPath() : mPhoto.getTitle();
        mTitleEditView.setText(title);
        mDescriptionEditView.setText(mPhoto.getDescription());

        mTitleTextView.setText(title);
        mDescriptionTextView.setText(mPhoto.getDescription());
    }

    @Override
    public View onCreateView(String name, Context context, AttributeSet attrs) {
        return super.onCreateView(name, context, attrs);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_photo, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_edit:
                editMode(true);

                return true;

            case R.id.action_remove:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Excluir");
                builder.setMessage("Tem certeza que deseja excluir essa foto?");

                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mProgressBar.setVisibility(View.VISIBLE);

                        PhotoDao photoDao = new PhotoDao(mContext);
                        photoDao.deletePhoto(mPhoto.getId());

                        File fdelete = new File(mPhoto.getPath().replace("file:", ""));
                        if (fdelete.exists()) {
                            fdelete.delete();
                        }

                        finish();
                    }
                });

                builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });


                AlertDialog dialog = builder.create();
                dialog.setCancelable(false);
                dialog.show();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        mProgressBar.setVisibility(View.VISIBLE);

        finish();

        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        mProgressBar.setVisibility(View.VISIBLE);
    }

    private void editMode(boolean isActivated) {
        if (isActivated) {
            mTitleEditView.setVisibility(View.VISIBLE);
            mDescriptionEditView.setVisibility(View.VISIBLE);
            mModeEditLinearLayout.setVisibility(View.VISIBLE);

            mTitleTextView.setVisibility(View.GONE);
            mDescriptionTextView.setVisibility(View.GONE);
        } else {
            mTitleEditView.setVisibility(View.GONE);
            mDescriptionEditView.setVisibility(View.GONE);
            mModeEditLinearLayout.setVisibility(View.GONE);

            mTitleTextView.setVisibility(View.VISIBLE);
            mDescriptionTextView.setVisibility(View.VISIBLE);
        }
    }

}
