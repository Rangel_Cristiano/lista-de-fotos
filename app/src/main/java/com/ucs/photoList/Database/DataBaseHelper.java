package com.ucs.photoList.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;

    private static final String DATABASE_NAME = "db_photos";

    public DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_PHOTO_TABLE = "CREATE TABLE " + PhotoDao.TABLE_PHOTO +
                "(" +
                    PhotoDao.KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    PhotoDao.KEY_TITLE + " INTEGER, " +
                    PhotoDao.KEY_DESCRIPTION + " TEXT, " +
                    PhotoDao.KEY_PATH + " TEXT " +
                ")";

        db.execSQL(CREATE_PHOTO_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {

    }
}