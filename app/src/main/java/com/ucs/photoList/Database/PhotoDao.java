package com.ucs.photoList.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;

import com.ucs.photoList.Models.Photo;

import java.util.ArrayList;

public class PhotoDao extends DataBaseHelper {

    // Table name
    public static final String TABLE_PHOTO = "Photo";

    // Table Columns names
    public static final String KEY_ID = "id";
    public static final String KEY_TITLE = "title";
    public static final String KEY_DESCRIPTION = "description";
    public static final String KEY_PATH = "path";

    public PhotoDao(Context context) {
        super(context);
    }

    public long savePhoto(Photo photo) {
        final SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(KEY_TITLE, photo.getTitle());
        values.put(KEY_DESCRIPTION, photo.getDescription());
        values.put(KEY_PATH, photo.getPath());

        long id = db.insertWithOnConflict(
                TABLE_PHOTO,
                null,
                values,
                SQLiteDatabase.CONFLICT_REPLACE);

        db.close();

        return id;
    }

    public ArrayList<Photo> getAllPhotos() {

        final SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT * FROM " + TABLE_PHOTO + " ORDER BY " + KEY_ID + " DESC";

        final Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor == null || cursor.getCount() == 0) {
            return null;
        }

        ArrayList<Photo> photosInputs = new ArrayList<>();
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                Photo photo = new Photo();

                photo.setId(cursor.getInt(0));
                photo.setTitle(cursor.getString(1));
                photo.setDescription(cursor.getString(2));
                photo.setPath(cursor.getString(3));

                photosInputs.add(photo);

                cursor.moveToNext();
            }
        }
        cursor.close();

        db.close();

        return photosInputs;
    }

    public long getLastId() {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_PHOTO, new String[] { KEY_ID },null, null, null, null, null, null);

        if (cursor == null || cursor.getCount() == 0) {
            return 0;
        }
        cursor.moveToLast();

        db.close();

        return cursor.getInt(0);
    }

    public void updatePhoto(Photo photo) {
        final SQLiteDatabase db = this.getWritableDatabase();

        String where = "id=?";
        String[] whereArgs = new String[] {String.valueOf(photo.getId())};

        ContentValues dataToInsert = new ContentValues();
        dataToInsert.put(KEY_TITLE, photo.getTitle());
        dataToInsert.put(KEY_DESCRIPTION, photo.getDescription());

        db.update(TABLE_PHOTO, dataToInsert, where, whereArgs);
    }

    public int deletePhoto(long id) {
        final SQLiteDatabase db = this.getWritableDatabase();
        int delete = db.delete(TABLE_PHOTO,
                KEY_ID + "=?",
                new String[]{String.valueOf(id)});
        db.close();

        return delete;
    }
}
