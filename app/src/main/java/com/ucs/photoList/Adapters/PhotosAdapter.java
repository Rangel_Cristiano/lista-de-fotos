package com.ucs.photoList.Adapters;

import android.app.Activity;
import android.content.ContentResolver;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ucs.photoList.Database.PhotoDao;
import com.ucs.photoList.Fragments.ItemFragment.OnListFragmentInteractionListener;
import com.ucs.photoList.Models.Photo;
import com.ucs.photoList.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PhotosAdapter extends RecyclerView.Adapter<PhotosAdapter.ViewHolder> {

    private List<Photo> mPhotos;
    private final OnListFragmentInteractionListener mListener;
    private Activity mActivity;

    public PhotosAdapter(
            List<Photo> photos,
            OnListFragmentInteractionListener listener,
            Activity activity) {

        mPhotos = photos;
        mListener = listener;
        mActivity = activity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mPhoto = mPhotos.get(position);

        String title = mPhotos.get(position).getTitle() == null || mPhotos.get(position).getTitle().isEmpty() ?
                mPhotos.get(position).getPath() : mPhotos.get(position).getTitle();

        holder.mTitleTextView.setText(title);
        holder.mDescriptionTextView.setText(mPhotos.get(position).getDescription());

        Uri selectedImage = Uri.parse(mPhotos.get(position).getPath());

        ContentResolver cr = mActivity.getContentResolver();
        Bitmap bitmap;
        try {
            bitmap = android.provider.MediaStore.Images.Media
                    .getBitmap(cr, selectedImage);
            bitmap = Bitmap.createScaledBitmap(bitmap, 250,250, true);

            holder.mImageView.setImageBitmap(bitmap);

        } catch (IOException e) {
            e.printStackTrace();
        }

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.onListFragmentInteraction(holder.mPhoto);
                }
            }
        });
    }

    public void addPhoto(Photo photo) {
        if (mPhotos != null)
            mPhotos.add(0, photo);
        else {
            mPhotos = new ArrayList<>();

            mPhotos.add(0, photo);
        }

        this.notifyItemInserted(0);
    }

    public void notifyDataChanged(ArrayList<Photo> photos) {
        mPhotos = photos;

        this.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mPhotos == null ? 0 : mPhotos.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final ImageView mImageView;
        public final TextView mTitleTextView;
        public final TextView mDescriptionTextView;
        public Photo mPhoto;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mImageView = view.findViewById(R.id.photo_image_view);
            mTitleTextView = view.findViewById(R.id.title_text_view);
            mDescriptionTextView = view.findViewById(R.id.description_text_view);
        }
    }
}
