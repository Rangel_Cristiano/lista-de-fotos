package com.ucs.photoList.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ucs.photoList.Adapters.PhotosAdapter;
import com.ucs.photoList.Database.PhotoDao;
import com.ucs.photoList.Models.Photo;
import com.ucs.photoList.R;

import java.util.ArrayList;

public class ItemFragment extends Fragment {

    private OnListFragmentInteractionListener mListener;
    private PhotosAdapter mPhotosAdapter;
    private RecyclerView mRecyclerView;

    public ItemFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_item_list, container, false);

        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            mRecyclerView = (RecyclerView) view;
            mRecyclerView.setLayoutManager(new LinearLayoutManager(context));

            PhotoDao photoDao = new PhotoDao(context);
            ArrayList<Photo> photos = photoDao.getAllPhotos();

            mPhotosAdapter = new PhotosAdapter(
                    photos,
                    mListener,
                    getActivity());

            mRecyclerView.setAdapter(mPhotosAdapter);
        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void addPhoto(Photo photo) {
        mPhotosAdapter.addPhoto(photo);
    }

    public void notifyDataChanged() {
        PhotoDao photoDao = new PhotoDao(getContext());
        ArrayList<Photo> photos = photoDao.getAllPhotos();

        mPhotosAdapter.notifyDataChanged(photos);
    }

    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(Photo item);
    }
}
